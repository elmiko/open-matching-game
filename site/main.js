/*
    Copyright (C) 2020 michael mccune

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Game from './modules/game.js';
import * as Playfield from './modules/playfield.js';

(function () {
  let game = null;
  let menucenter = document.getElementById('menu-center');
  let newgame = document.getElementById('new-game');
  let about = document.getElementById('about');
  let iconlist = [];

  fetch('icons.json').then(response => response.json()).then(data => iconlist = data);

  let updatelistener = {
    set: function(obj, prop, value) {
      obj[prop] = value;
      update();
      return true;
    },
  };

  function configureMenuButtons() {
    newgame.onclick = function (e) {
      // TODO check for game in progress or something

      game = Game.newGame(iconlist, updatelistener);
      Playfield.initialize(game);
      Playfield.update(game);
      Playfield.show();
      update();
    };

    about.onclick = function (e) {
      let abouttext = document.getElementById('about-text');
      let background = document.getElementById('about-background');
      abouttext.style.display = 'block';
      background.style.display = 'block';
      function close(e) {
        abouttext.style.display = 'none';
        background.style.display = 'none';
      }
      abouttext.onclick = close;
      background.onclick = close;
    }
  }

  function update() {
    let newtext = 'Turn: ';
    if (game != null) {
      newtext += game.turn;
    } else {
      newtext += 0;
    }
    menucenter.innerText = newtext;
    Playfield.update(game);
  }

  function init() {
    configureMenuButtons();
    update();
  }

  init();
})();
