/*
    Copyright (C) 2020 michael mccune

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export {
  activatePiece,
  newGame,
};

function activatePiece(idx, game) {
  if (game.playfield[idx].hidden === true) {
    if (game.activatedPieces < 2) {
      game.playfield[idx].hidden = false;
      game.activatedPieces = game.activatedPieces + 1;
      if (game.activatedPieces == 2) {
        endTurn(game);
      }
    }
  }
}

function endTurn(game) {
  if (game.activatedPieces != 2) {
    return;
  }
  // TODO check values, and do stuff depending on what happens

  let selectedpieces = game.playfield.filter(piece => piece.hidden == false);
  if (selectedpieces.length != 2) {
    console.log('unexpected pieces length');
  }

  let clear = false;
  if (selectedpieces[0].icon == selectedpieces[1].icon) {
    clear = true; 
  }

  window.setTimeout(function () {
    let pfcopy = game.playfield.slice()
    pfcopy.forEach(function (i) {
      if (i.hidden == false) {
        i.hidden = true
        if (clear == true) {
          i.clear = true;
        }
      }
    });
    game.playfield = pfcopy;
    game.activatedPieces = 0;
    game.turn = game.turn + 1;
  }, 1500);
}

function fiftyfifty() {
  if (randInt(2) == 0) {
    return -1;
  }
  return 1;
}

function newGame(iconlist, updatelistener) {
  let pieces = 64;
  let reducediconlist = [];
  let iconlistcopy = iconlist.slice();
  for (let i = 0; i < pieces / 2; i++) {
    iconlistcopy.sort(fiftyfifty);
    reducediconlist.push(iconlistcopy.pop());
  }
  let finaliconlist = []
  finaliconlist = finaliconlist.concat(reducediconlist.slice(), reducediconlist.slice());
  finaliconlist.sort(fiftyfifty);

  let playfield = [];
  for (let i = 0; i < 64; i ++) {
    playfield[i] = new Proxy({
      icon: finaliconlist[i],
      cleared: false,
      hidden: true,
    }, updatelistener);
  }

  let game = {
    activatedPieces: 0,
    iconlist: finaliconlist,
    pieces: pieces,
    playfield: playfield,
    turn: 1,
  };
  return new Proxy(game, updatelistener);
}

function randInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
