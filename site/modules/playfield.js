/*
    Copyright (C) 2020 michael mccune

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as Game from './game.js';

export {
  hide,
  initialize,
  show,
  update,
};

let playfield = document.getElementById('playfield');
let playfieldwrapper = document.getElementById('playfield-wrapper');

function hide() {
  playfieldwrapper.style.setProperty('display', 'none');
}

function initialize(game) {
  function gamepieceClickDecorator(idx) {
    let gamepieceClick = function (e) {
      Game.activatePiece(idx, game);
    }
    return gamepieceClick
  };

  playfield.replaceChildren();

  let gamepieces = [];
  for (let i = 0; i < game.pieces; i++) {
    let gamepiece = document.createElement('div');
    gamepiece.classList.add('gamepiece');
    gamepiece.onclick = gamepieceClickDecorator(i);
    let pieceimg = document.createElement('img');
    pieceimg.setAttribute('src', game.iconlist[i]);
    pieceimg.setAttribute('width', '64px');
    pieceimg.setAttribute('height', '64px');
    pieceimg.style.setProperty('opacity', '0');
    gamepiece.append(pieceimg);
    gamepieces.push(gamepiece);
  }
  playfield.append(...gamepieces);
}
  
function show() {
  playfieldwrapper.style.setProperty('display', 'grid');
}

function update(game) {
  if (game == null) {
    return
  }
  game.playfield.forEach(function (i, idx) {
    let pieceimg = playfield.children[idx].querySelector('img');
    if (pieceimg == null) {
      console.log('no image for piece found ' + idx);
      return;
    }
    let opacity = pieceimg.style.getPropertyValue('opacity');
    if (i.clear) {
      playfield.children[idx].style.setProperty('opacity', 0);
      playfield.children[idx].onclick = null;
      return
    }
    if (i.hidden) {
      if (opacity == 1) {
        pieceimg.style.setProperty('opacity', 0);
      }
    } else {
      if (opacity == 0) {
        pieceimg.style.setProperty('opacity', 1);
      }
    }
  });
}
