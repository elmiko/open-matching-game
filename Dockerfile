FROM registry.access.redhat.com/ubi8/ubi

RUN dnf install -y nginx && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

COPY site /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
