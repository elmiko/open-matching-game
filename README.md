# Open Matching Game

[![Docker Repository on Quay](https://quay.io/repository/elmiko/open-matching-game/status "Docker Repository on Quay")](https://quay.io/repository/elmiko/open-matching-game)

A game about matching images.

[Try it out here](https://omg.opbstudios.com)

Open Matching Game is an HTML based static website which contains a matching game.
The player takes turns exposing pairs of hidden images in attempt to locate all the matches.
The `site` directory contains static files that are used to host the game. It requires
a simple HTTP server.

## Container Quickstart

Run the latest version from a container image:
```
podman run --rm -it -p 8000:80 quay.io/elmiko/open-matching-game:latest
```
then point a browser at `http://localhost:8000`.

## Development

Use a container to generate a local image and run it:
```
podman build -t omg .
podman run --rm -it -p 8000:80 localhost/omg
```

Run a simple Python HTTP server for local development:
```
make serve
```
