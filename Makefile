serve:
	python -m http.server --directory site

build-image:
	podman build -t omg .

icon-data:
	@python -c "import json; import os; print(json.dumps([f'/img/icons/{bfn}' for bfn in os.listdir('./site/img/icons')], indent=2, sort_keys=True))"
